#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# alias python="python2"
# PS1='[\u@\h \W]\$ '
PS1="\\u@\h \w:\$(git branch 2>/dev/null | grep '^*' | colrm 1 2) \$ "

alias ls='ls --color=auto'
alias ll='ls --color=auto -alh'
alias ai2='cd ~/Projects/appinventor-sources'
alias interject='~/Documents/COMP-1/random-bullshit/textfile/a.out'
alias proj='cd ~/Projects'
alias hw='cd ~/Documents'
alias android='aft-mtp-mount ~/OP6'
alias school='cd ~/Schoolwork/2020_Fall'
alias is='cd ~/Projects/rSENSE'
alias i3conf='vim ~/.config/i3/config'

alias config='/usr/bin/git --git-dir=$HOME/.i_use_arch_btw --work-tree=$HOME'
# (wal -r &)


# Add python user bin to PATH
export PATH="$PATH:$HOME/.local/bin/"

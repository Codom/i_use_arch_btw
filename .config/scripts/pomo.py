#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright 2021 Christopher Odom <christopher.r.odom@gmail.com
#
# Distributed under terms of the MIT license.
# 
# TODO: Database storage
# TODO: Progress report

"""
Just a cli pomodoro timer
"""

import time
import subprocess
from pynotifier import Notification

# Global knobs
SESSION = 25   * 60
BREAK   = 5    * 60
L_BREAK = 15   * 60

main_notif = Notification(title="Work started", description="Focus for {} minutes".format(SESSION/60),
    duration=5,
)

break_notif = Notification(title="Work ended", description="Take {} minutes to breath".format(BREAK/60),
    duration=5,
)

long_b_notif = Notification(title="Mini session ended", description="Take {} minutes to breath".format(L_BREAK/60),
    duration=5,
)

end_notif = Notification(title="Focus session end", description="Take a longer break",
    duration=5,
)

# global because lazy
session_cnt = 0

def dmenu(options):
    cmd = ['rofi', '-dmenu']
    return subprocess.run(cmd,input=options,stdout=subprocess.PIPE).stdout.decode('utf-8')

def timed_wait(seconds):
    time.sleep(seconds)

def main():
    pomodoro()

def pomodoro():
    global session_cnt
    b_dmenu = "{}m break?\n".format(int(BREAK/60))
    lb_dmenu = "{}m break?\n".format(int(L_BREAK/60))
    skip_dmenu = "Continue Work?\n"
    end_dmenu = "End session?\n"
    start_work = "Start work?\n"
    dmenu_all = "".join((b_dmenu, lb_dmenu, skip_dmenu, end_dmenu)).encode('utf-8')
    dmenu_first = "".join((start_work, b_dmenu, lb_dmenu, end_dmenu)).encode('utf-8')

    prompt = dmenu(dmenu_first)
    prompt = skip_dmenu if prompt == start_work else prompt
    while(True):
        if prompt == b_dmenu:
            break_notif.send()
            timed_wait(BREAK)
        elif prompt == lb_dmenu:
            long_b_notif.send()
            timed_wait(L_BREAK)
        elif prompt == skip_dmenu:
            main_notif.send()
            timed_wait(SESSION)
            session_cnt = session_cnt + 1
        elif prompt == end_dmenu:
            print("Completed {} sessions".format(session_cnt))
            break
        prompt = dmenu(dmenu_all)

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt as e:
        print("Completed {} sessions".format(session_cnt))

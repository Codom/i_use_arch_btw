if status is-interactive
    # Commands to run in interactive sessions can go here
    fish_add_path ~/.local/bin
    alias config='/usr/bin/git --git-dir=$HOME/.i_use_arch_btw --work-tree=$HOME'
end

# Created by `userpath` on 2022-11-18 17:25:46
set PATH $PATH /home/codom/.local/bin

#! /bin/sh
#
# cycle_backgrounds.sh
# Copyright (C) 2023 Christopher Odom <christopher.r.odom@gmail.com>
#
# Distributed under terms of the MIT license.
#

# Specify the folder containing the images
IMAGE_FOLDER="$HOME/Media/Pictures/Wallpapers"

# Function to set the desktop background to a random image
set_random_background() {
  # Get a random image file from the folder
  image_file1=$(find "$IMAGE_FOLDER" -type f \( -name '*.jpg' -o -name '*.png' \) -print0 | shuf -z -n 1)

  # Set the desktop background using feh
  feh --bg-fill --no-xinerama "$image_file1"
}

# Initial setup
set_random_background

# Loop to change the background every 30 minutes
while true; do
  sleep 1800 # Wait for 30 minutes
  set_random_background
done

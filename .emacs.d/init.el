;; Set up package.el to work with MELPA
(require 'package)
(add-to-list 'package-archives
	                  '("melpa" . "https://melpa.org/packages/"))
(package-initialize)
;; (package-refresh-contents)

;; Download Evil
(unless (package-installed-p 'evil)
    (package-install 'evil))

;; Enable markdown-mode
(require 'markdown-mode)
(setq markdown-enable-math t)

;; Setup evil mode and evil collection
(setq evil-want-integration t) ;; This is optional since it's already set to t by default.
(setq evil-want-keybinding nil)
(require 'evil)
(require 'org-evil)
(when (require 'evil-collection nil t)
    (evil-collection-init))
(evil-mode 1)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(display-line-numbers 'relative)
 '(evil-overriding-maps '((nil)))
 '(org-agenda-files '("~/Schoolwork/orgmode/fall.org"))
 '(package-selected-packages
   '(org-evil evil-collection markdown-mode markdownfmt undo-fu undo-fu-session undo-tree evil)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
